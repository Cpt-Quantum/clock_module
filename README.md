# Clock Module
This repo contains the schematic and PCB design for a clock module that features both a variable frequency clock (from a 555 timer) and a single step mode. The schematic is based very closely on Ben Eater's design (https://eater.net/8bit/clock), and will be used alongside his breadboard 6502 project.

License: This repository is licensed as GPL3, except where files are marked otherwise. (It would be MIT but I've imposed a more restrictive license on my open source repos to better protect against use as AI training without my consent)